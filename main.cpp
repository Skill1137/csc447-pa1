/***************************************************************************//**
 * @file 
 * @brief 
 *
 * @mainpage Program 1 - Evolutionary Algorithms
 *
 * @section course_section Course Information
 *
 * @authors Alex Muchow, Brock Benson, Hafiza Farzami
 *
 * @date February 24, 2014
 *
 * @par Instructor:
 *         Dr. Weiss
 *
 * @par Course:
 *         CSC 447 - Section M001 - 09:00 a.m.
 *
 * @par Location:
 *         McLaury - Room 304
 *
 * @section program_section Program Information
 *
 * @details The following program solves a given sudoku puzzle firstly filling
 * the predetermined cells, and then using the genetic algorithm techniques: 
 * creating initial population, crossover, mutation, etc. 
 * 
 * @experimental After testing we found that a population of around 1000-10000
 * with a generation size of 1000. Selection rate should be between 15% - 25%.
 * mutation should be as high as possible, due to the nature of the problem it
 * is very easy to find local mins, high mutation rate helps jump out
 * of these states. Restart threshholds should generally be between 50-100.
 * Due to our solutions converging on local minimums quickly. 
 *
 * An idea for a more sophisticated fitness function that we did not have a
 * chance to implement would be testing it against the solution. In theory,
 * we would compare each cell to the solution and take the absolute value of
 * the difference, and add that to an overall sum for the fitness of that puzzle.
 * In this way, each cell would converge on its correct value as opposed to having
 * the rows and columns converge on their validity. 
 *  
 * @section compile_section Compiling and Usage
 *
 * @par Compiling Instructions:
 *      None
 *
 * @section todo_bugs_modification_section Todo, Bugs
 *
 * @bug  none
 *
 * @todo none
 *
 ******************************************************************************/

#include "functions.h"

/**************************************************************************//**
 * @author Benson, Farzami, Muchow
 *
 * @par Description:
 * Takes in a puzzle with optional command line arguments. Fills in all
 * predetermined squares. Random filled (but not correct) variations of 
 * of the initial puzzle. Subsequent generations are created by
 * selecting random puzzles within the selection rate and crossing them.
 * The children puzzles are then (possibly) mutated. Each generation is
 * sorted by fitness. Selection only accepts a percentage of the most fit. 
 * The cycle breaks after the maximum number of generations is hit, or a 
 * valid solution (fitness = 0) is found. A reset point has also been 
 * implemented which allows the program to "retry" from the starting point.
 * This allows the program to break out of local minimums and maximums. 
 * Displays initial parameters, initial configuration, predetermined squares,
 * best/worst puzzle in each generation, and final/best solution found. 
 *
 * @param[in] argc	- number of params
 * @param[in] argv[0]	- program
 * @param[in] argv[1]	- input file
 * 	// Optional arguments
 * @param[in] argv[2]	- population size
 * @param[in] argv[3]	- generation size
 * @param[in] argv[4]	- selection rate
 * @param[in] argv[5]	- mutation rate
 * @param[in] argv[6]	- reset threshold
 *
 * @returns  0		- program ran succesfully 
 * @returns -1		- usage error
 * @returns -2		- file error
 * @returns -3		- invalid selection rate
 *
 *****************************************************************************/
int main( int argc, char ** argv ) 
{
	int population_size = 1000; 	// number of puzzles in each population
	int generation_size = 1000; 	// number of generations to to run before quiting
	int mutation_rate = 75; 	// percent from 0-100 chance that a puzzle will be mutated 
	float selection_rate = .50;	// percent of the puzzles that are allowed to breed
	int restart_threshold = 100; 

	vector<char> givens;
	int dim;
	
	if( argc < 2 )
	{
		cout << "Usage: ga puzzle.txt population_size generations selection_rate mutation_rate reset_threshold" << endl;
		return -1;
	}

	ifstream fin( argv[ 1 ] ); 

	if( !fin )
	{
		cout << "Could not open the input file." << endl;
		return -2;
	}

	// optional command line arguments
	if( argc > 2 )
	{
		if( argc > 3 )
		{
			if ( argc > 4 )
			{
				if ( argc > 5 )
				{
					if ( argc > 6 )
						restart_threshold = atoi(argv[6]); 
					mutation_rate = atof(argv[5])*100; 
				}
				selection_rate = atof(argv[4]); 
				if ( selection_rate > 1 )
				{
					cerr << "Error - Selection Rate must be 1 or smaller" << endl; 
					return -3; 
				}
			}	
			generation_size = atoi(argv[3]); 
		}			
		population_size = atoi(argv[2]);
	}

	// display arguments
	cout 	<< "\nSudoku: " << argv[1] << endl
		<< "Population Size: " << population_size << endl
		<< "Number of Generations: " << generation_size << endl 
		<< "Selection Rate: " << selection_rate << "%\n"
		<< "Mutation Rate: " << mutation_rate << "%\n"
		<< "Restart Threshold: " << restart_threshold << endl;

	// read in the puzzle, return dimension of puzzle
	// givenInts holds integer representation of puzzle
	dim = encoding( fin, givens );

	cout << "\nInitial Configuration:\n"; 
	display(givens);
	
	cout << "\nFilling predetermined Squares:\n";
	predetSquares( givens );
	display(givens);

	// Initialize Random Seed
	srand (time(NULL));
	
	cout << "\nStarting Genetic Algorithm\n"; 

	int i = 0;
	reset:		// reset point

	// generate initial population
	vector< puzzle > population = generatepopulation( givens, population_size ); 
	// epochs
	int parentA; 	// index to parents in population vector
	int parentB;
	for ( i; i < generation_size; i++ )
	{
		if ( i % restart_threshold == 0 && i != 0 )
		{
			i++;
			cout << "Restarting Genetic Algorithm\n"; 
			goto reset;
		}
		
		vector<puzzle> offspring; // initialize a temporary vector to hold the offspring	
		sort( population.begin(), population.end(), cmp_by_rank);


		for ( int k = 0; k < population_size; k++ )
		{
			parentA = rand() % population_size * selection_rate;	// some puzzles may breed multiple times
			parentB = rand() % population_size * selection_rate;	// some puzzles may not be selected at all
										// similar to having them die off randomly

			puzzle child = crossover( population[parentA].grid, population[parentB].grid );	//create a child
			child = mutate( givens, child, mutation_rate ); 				//mutate the child
			child.fitness = fitness( child.grid ); 						//determine fitness

			// push offspring onto vector
			offspring.push_back( child ); 
		}
	
		// display best/worst puzzles from this generation
		cout 	<< "Generation\t" << i << "\tBest Score: " << population[0].fitness
			<< "\t\tWorst Score: " << population[population_size-1].fitness << endl; 
		
		// break if we have a valid solution
		if ( population[0].fitness == 0 || i+1 == generation_size)
			break;

		// replace old population with offspring
		population.clear(); 
		for ( int k = 0; k < population_size; k++ )
			population.push_back( offspring[k]);

	}
	
	// display final puzzle ( may not be a correct solution )
	cout << "\nFinal Solution Fitness: " << population[0].fitness << "\n\n";
	population[0].grid=subGrids( population[0].grid ); 
	display( population[0].grid); 

	return 0;
}
