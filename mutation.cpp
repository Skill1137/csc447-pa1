#include<iostream> 
#include<vector>
#include<algorithm>
#include<time.h>
#include<stdlib.h>

using namespace std;

void mutation( vector<int> initial, vector<int> &puz, int mutation_rate )
{
	if ( rand() % 100 < mutation_rate )
	{
		int index1;
		int index2; 
		int square1; 
		int square2; 
		int x, y; 

		//pick an index that was not predetermined
		do{
			index1 = rand() % 81;		
			x = index1 / 9; 
			y = index1 % 9;
			square1 = square( x, y );
		}while( initial[index1] != 0 );
		
		//pick another index that was not predetermined, and not
		//equal to the first index. Must be in the same square. 
		do{
			index2 = rand() % 81;
			x = index2 / 9; 
			y = index2 % 9; 	
			square2 = square( x, y );
		}while( initial[index2] != 0 && index1 != index2 && square1 == square2 );

		//swap selected values
		swap( puz[index1], puz[index2] ); 
	}
}

// assuming row and col also start at 1.  
int square(int row,int col)
{
 
    if(row < 3 && col < 3)
        return 1;
    if(row < 3 && col < 6)
        return 2;
    if(row < 3 && col < 9)
        return 3;
 
    if(row < 6 && col < 3)
        return 4;
    if(row < 6 && col < 6)
        return 5;
    if(row < 6 && col < 9)
        return 6;
 
    if(row < 9 && col <3)
        return 7;
    if(row < 9 && col <6)
        return 8;
    if(row < 9 && col <9)
        return 9;
}


int main()
{
	
	
	vector<int> 
	for ( int i = 0; i < 81; i++ )
		if( rand()%50 < 100 )
			puz.push_back( 0 );
		else 
			puz.push_back( 1 );

	for ( int i = 0; i < 81; i++ )
	{
		if( i % 9 == 0 )
			cout << endl; 
		cout << i; 
	}

 	/*
	int x, y; 
	for ( int i = 1; i < 82; i++ )
	{	
		x = i / 9;
		y = i % 9; 

		cout << square(x, y); 
		if ( i-1 % 3 == 0 )
			cout << " " ;
		if ( i-1 % 9 == 0 )
			cout << endl; 
	}
	*/
}



