/************************************************************************
*                ***** functions.h *****
* 	The following program solves a given sudoku puzzle firstly filling
* the predetermined cells, and then using the genetic algorithm techniques: 
* creating initial population, crossover, mutation, etc. 
*
*
* Authors: Alex Muchow, Brock Benson, Hafiza Farzami
* Class:  CSC 447 Artificial Intelligence 
* Date:   February 24, 2014
*************************************************************************/

#ifndef functions_h
#define functions_h

// include files
#include<iostream> 
#include<vector>
#include<algorithm>
#include<time.h>
#include<stdlib.h>
#include<fstream>
#include <cmath>
#include <string> 

using namespace std;

// struct
struct puzzle
{
	vector<char> grid;
	int fitness;
	puzzle():fitness(0){}
};

// Predetermined Square Functions
void predetSquares( vector<char> &givens ); 
void updateSquares( vector<char> givens, bool avail[][9] );
int square(int index);

// Genetic Operators
puzzle mutate(vector<char> initial, puzzle puz, int mutation_rate);
puzzle crossover( vector<char> parentOne, vector<char> parentTwo );
vector< puzzle > generatepopulation( vector<char> initial, int population_size );

// Puzzle Operations
void display( vector<char> puz );
vector<char> subGrids(vector<char> puz);
vector<char> fillpuzzle( vector<char> puz );
int encoding( ifstream &fin, vector<char> &givens );


// Fitness function and its helper functions
int fitness( vector<char> givens );
bool cmp_by_rank( const puzzle a, const puzzle b );

#endif
