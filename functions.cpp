#include "functions.h"


/******************************************************************************
*			Predetermined Square FUnctions
******************************************************************************/
/**************************************************************************//**
 * @author Alex Muchow
 *
 * @par Description:
 * Fills in the naked and hidden singles of a given puzzle. Walks through each
 * cell and removes invalid cell possibilities. It goes through and first
 * updates the possibilities. then it begins to fill in the naked singles. Naked
 * singles are the cells that only have one possible number to go in them.
 * Whenever it fills in a square, it reupdates the possibilities and restarts
 * the index at 0. Next, the function fills in the "Hidden" singles. Hidden
 * singles are filled in when there is can one number can only go one spot 
 * in any given cell of a row, column or subgrid. If it fills in a square, 
 * it calls the updateSquares function to update the boolean array and resets
 * the index back to 0.
 *
 * @param[in] &givens - uncompleted original puzzle
 *
 * @returns none
 *
 *****************************************************************************/
void predetSquares( vector<char> &givens ) 
{
	bool aval[81][9];
	// set all values to 1
	for ( int i = 0; i < 81; i++ )
		for ( int j = 0; j < 9; j++ )	
			aval[i][j] = 1;

	updateSquares(givens, aval);
	
	int sum;
	//naked singles
	for(int i = 0; i < 81; i++)
	{
		//walk through each and find the number of available numbers in each spot.
		for(int k = 0;  k < 9; k++)
		{
			//add one to sum if not available
			if(aval[i][k] == 0)
				sum++;
		}
		//check if there is only one spot available
		if(sum == 8)
		{	
			//if there is only one available spot, walk through until 
			for(int k = 0; k < 9; k++)
				if(aval[i][k]!= 0)
					givens[i] = char(k+49); //place in the corresponding char
		
			updateSquares(givens, aval);//update squares after placing a char in a grid
			i = 0;	//reset i to zero to walk through it
		}
			
		sum = 0; //reset sum to 0 for checking next 
	}

	//fill in hidden singles

	//walk through each cell
	for(int i = 0; i < 81; i++)
	{
		//walk through each availability
		for(int k = 0; k < 9; k++)
		{
			int sum = 0;
			if(aval[i][k] != 0)
			{
				//check rows
				for(int z = 0; z < 9; z++)
					if(aval[(i/9)*9 + z][k] == 0)
						sum++;
				if(sum == 8)
				{
					givens[i] = char(k+49); //place in the corresponding char
					updateSquares(givens, aval);//update squares after placing a char in a grid
					i = 0;
					break;
				}
				sum =0;

				//check columns
				for(int z = 0; z < 81; z+=9)
					if(aval[(i % 9) + z][k] == 0)
						sum++;
				if(sum == 8)
				{
					givens[i] = char(k+49); //place in the corresponding char
					updateSquares(givens, aval);//update squares after placing a char in a grid
					i = 0;
					break;
				}
				sum = 0;

				//check subgrids
				for(int z = 0; z < 81; z++)
					if(square(z) == square(i))
						if(aval[z][k] == 0)
							sum++;
				if(sum == 8)
				{
					givens[i] = char(k+49); //place in the corresponding char
					updateSquares(givens, aval);//update squares after placing a char in a grid
					i = 0;
					break;
				}
			}
		}
	}
}
/**************************************************************************//**
 * @author Alex Muchow
 *
 * @par Description:
 * Walks through the puzzle cell by cell if there is a number in the cell, it
 * checks off the digit availibility in the row, column and subgrid that the
 * index is in
 *
 * @param[in] givens - given puzzle with numbers and 0's
 * @param[in] aval - Boolean array of availability
 *
 * @returns none
 *
 *****************************************************************************/
void updateSquares( vector<char> givens, bool aval[][9] )
{
	//for each cell
	for( int i = 0; i < 81; i++)
	{
		//if cell has a digit d
		if(givens[i] != '0')
		{
			//clear availability in that space as it is taken
			for(int k=0; k < 9; k++)
				aval[i][k] = 0;
			//clear availability in row for digit d
			for(int k = 0; k < 9; k++)
				aval[(i/9)*9 + k][givens[i]-'1'] = 0;
			//clear availability in column for digit d
			for(int k = 0; k < 81; k+=9)
				aval[(i % 9) + k][givens[i]-'1'] = 0;
			//clear availability in subgrid for digit d
			for(int k = 0; k < 81; k++)
				if(square(k) == square(i))
					aval[k][givens[i]-'1'] = 0;
		}
	}
}


/**************************************************************************//**
 * @author Alex Muchow
 *
 * @par Description:
 * Takes an index to the 1D sudoku array and returns the subgrid number. 
 * 
 * @param[in] index - index (0-80) to the position in the 1D array
 *
 * @returns int - number of the current subgrid
 *
 *****************************************************************************/
int square(int index)
{
    int row = index / 9;
    int col = index % 9;
    if(row < 3 && col < 3)
        return 1;
    if(row < 3 && col < 6)
        return 2;
    if(row < 3 && col < 9)
        return 3;
 
    if(row < 6 && col < 3)
        return 4;
    if(row < 6 && col < 6)
        return 5;
    if(row < 6 && col < 9)
        return 6;
 
    if(row < 9 && col <3)
        return 7;
    if(row < 9 && col <6)
        return 8;
    if(row < 9 && col <9)
        return 9;
}
/******************************************************************************
*			     Genetic Operators
******************************************************************************/
/**************************************************************************//**
 * @author Brock Benson
 *
 * @par Description: This function randomly mutates a puzzle based on the 
 * mutation_rate. It picks two squares within a sub grid and swaps them. However,
 * it does not change predetermined values. If the function is unable to find a
 * valid cell to swap after 100 tries, it will break from the function. This 
 * prevents the mutate function from entering an infinite loop when there are
 * no valid cells to swap. 
 *
 * @param[ in ] 	initial - the initial puzzle
 * @param[ in ]		mutation_rate - mutation rate
 * @param[ in, out ] 	puz - the puzzle to be mutated
 *
 * @returns 		puz - the mutated puzzle
 *
 *****************************************************************************/
puzzle mutate(vector<char> initial, puzzle puz, int mutation_rate)
{
	initial = subGrids(initial);
	if ( rand() % 100 < mutation_rate )
	{
		int subgrid = rand() % 9;
		int index1;	// index to first valid cell
		int index2; 	// index to second valid cell
		int i = 0; 	// used to force return (if needed)

		// pick a number from the subgrid, make sure it isn't a 
		// predetermined value.
		do{	
			index1 = (subgrid * 9) + (rand() % 9);
			i++; 
	
			if ( i > 100 )		// break if no
				return puz; 	// valid cells

		}while( initial[index1] != '0' );
	

	
		// pick a second number in the subgrid
		do{
			index2 = (subgrid * 9) + (rand() % 9);

			i++; 
			if ( i > 100 )		// break if no
				return puz; 	// valid cells

		}while( initial[index2] != '0' );
	
		swap( puz.grid[index1], puz.grid[index2] ); 
	}

	return puz; 	// return the (possibly) mutated puzzle
}

/**************************************************************************//**
 * @author Hafiza Farzami
 * @par Description:This function takes in two parents, and creates an offspring 
 * by taking a random number of subgrids and the rest from the second parent.
 *
 * @param[ in ]		parentOne - the first parent
 * @param[ out ] 	parentTwo - the second parent
 *
 * @returns 		puzzle - an offspring with its fitness rank
 *
 *****************************************************************************/
puzzle crossover( vector<char> parentOne, vector<char> parentTwo )
{
	int randInt = rand() % 8 + 1;
	puzzle offspring;

	for( int i = 0; i < ( randInt * 9 ); i++ )
		offspring.grid.push_back( parentOne[ i ] );

	for( int j = ( randInt * 9 ); j < 81; j++ )
		offspring.grid.push_back( parentTwo[ j ] );

	offspring.fitness = fitness( offspring.grid ); 
	return offspring;
}
/**************************************************************************//**
 * @author Brock Benson
 *
 * @par Description:
 * generate population creates a vector of puzzles based on random variations
 * of the initial puzzle given to the program.
 *
 * @param[in] initial - uncompleted original puzzle
 * @param[in] population_size - number of puzzles in the population
 *
 * @returns population - vector of puzzles based on random variations of
 * 			 the initial puzzle
 *
 *****************************************************************************/
vector< puzzle > generatepopulation( vector<char> initial, int population_size )
{
	vector< puzzle > population;
	puzzle temp; 
	
	for ( int i = 0; i < population_size; i++ )
	{
		temp.grid = subGrids(initial);
		temp.grid = fillpuzzle( temp.grid ); 
		temp.fitness = fitness( temp.grid ); 
		population.push_back( temp ); 
	}

	return population; 
}


/******************************************************************************
*			     Puzzle Operations
******************************************************************************/
/**************************************************************************//**
 * @author Alex Muchow
 *
 * @par Description:
 * output the puzzle in a somewhat nicely formatted manner, just for you.
 *
 * @param[in] puz - The puzzle to be printed
 *
 * @returns none
 *
 *****************************************************************************/
void display( vector<char> puz )
{
    for ( int i = 1; i < 82; i++ )
    {
	if( puz[i-1] == '0' )
		cout << '-'; 
	else
        	cout << puz[i-1];
	//vertical line between subgrids
        if( i % 3 == 0 && i % 9 != 0)
            cout << '|';
	//end of row
        if( i % 9 == 0 )
            cout << endl;
	//output between horizontal subgrids
        if(i % 27 == 0 && i != 81)
            cout << "---+---+---\n";
    }

}

/**************************************************************************//**
 * @author Alex Muchow
 *
 * @par Description: The 'subGrids' function takes in a puzzle that is in 
 * "row by row" format, and it rearranges it to subgrid by subgrid format. 
 *
 * @param[ in ]		puz - the sudoku puzzle
 *
 * @returns 		puz2 - the rearranged puzzle
 *
 *****************************************************************************/
vector<char> subGrids(vector<char> puz)
{
    int i=0;     //index to the first subgrid
    int k=i + 3; //index to the second subgrid
    vector<char> puz2;

    for(i; k != 63; i+=9)
    {
        // 27 and 30, 54 and 57, 81 and 84 are the index's
        // which trigger moving to the second and third columns of grids 
        if(i == 27|| i == 30 || i ==54 || i == 57 || i == 81 || i == 84)
        {
            i = k;//set i to the start of the next subgrid
            k +=3;//increment k to the next subgrid
        }
        if( i == 33 && k == 9) // if the index needs to change to the second row of subgrids
        {
            k = 30;             //set k as an index to the 5th subgrid
            i = 27;             //set i as an index to the 4th subgrid
        }
        if(i == 60 && k == 36)//if the index needs to go to the last row of subgrids
        {
            k = 57;//set k as the index to the 8th subgrid
            i = 54;//set i as the index to the 7th subgrid
        }
reloop:
        for(int j=i; j < i+3; j++)//walk through each row of the subgrids
            puz2.push_back(puz[j]);//copy over to the other vector

        if(k == 63 && i < 78) //special case for the last subgrid
        {
            // 60 61 62
            // 69 70 71
            // 78 79 80
            //for this last subgrid, because the index would overstep the bounds of 
            //the loop, a special case was needed to catch the last numbers
            //This if statement will catch the last three iterations needed to
            //go through the last subgrid, it uses a goto to avoid rewriting
            //code that has been already written
            i+=9; // move down a row in the subgrid
            goto reloop; //loop through the subgrid row copying
        }

    }

    return puz2;
        
}
/**************************************************************************//**
 * @author Brock Benson
 *
 * @par Description:
 * This function takes an incompleted puzzle in subGrid form. Each subgrid
 * is scanned for valid numbers that can be placed in the open slots. The
 * subgrid is then filled randomly with the remaining numbers. Each subgrid
 * is ensured to have 1-9 with no duplicates. fillpuzzle then returns the 
 * puzzle.
 * @param[in] puz - puzzle to fill
 *
 * @returns puz - randomly filled puzzle
 *
 *****************************************************************************/
vector<char> fillpuzzle( vector<char> puz )
{
	vector<int> list;  
	
	int index = 0; 
	
	// for each subgrid i
	for ( int i = 0; i < 9; i++ )
	{

		// initialize list to {1,2,...,9}
		for ( int j = 0; j < 9; j++ )
			list.push_back(j+1);

		// run through each element of the subgrid
		for ( int k = 0; k < 9; k++ )
		{
			index = i*9 + k; 		//index in puz (0-80)

			// if not 0, find the number in the vector and remove it
			if ( puz[index] != '0' )
			{
				for ( int l = 0; l < list.size(); l++ )
					if ( list[l] == puz[index] - '0' )
						list.erase(list.begin()+l); 
			}
		}

		// walk through subgrid, fill in zeros with a random
		// number from the list
		int num; 
		for ( int k = 0; k < 9; k++ )
		{
			index = i*9 + k; 

			if ( puz[index] == '0' ) 
			{
				num = rand() % list.size(); 
				puz[index] = list[num] + '0';
				list.erase(list.begin() + num ); 
			}
		}

	}
		
	return puz; 
}



/**************************************************************************//**
 * @author Hafiza Farzami
 *
 * @par Description: The function 'encoding', reads in the dimensions of a 
 * sudoku puzzle, and saves it. It also reads in the contents of the puzzle
 * and inserts them in to a vector of charactors.  
 *
 *
 * @param[ in ]		fin - the input file
 * @param[ in, out ] 	givens - the vector containing the initial puzzle  
 *
 * @returns 		dim - the dimension of the puzzle
 *
 *****************************************************************************/
int encoding( ifstream &fin, vector<char> &givens )
{
	char x;
	fin >> x >> x;
	int dim = x - '0';
	while( fin >> x )
	{
		if(x == '-')
			x='0';
		givens.push_back(x);
	}

	return dim;
}
/******************************************************************************
*			Fitness Functions / Helpers
******************************************************************************/
/**************************************************************************//**
 * @author Benson, Farzami, Muchow
 *
 * @par Description: Given a puzzle, the following function assigns a fitness
 * rank to it. The fitness of the puzzle is determined not only by the number of
 * duplicates in each row, but also each column.
 *
 * @param[ in ]		initial - the puzzle to be tested		
 *
 * @returns 		sum - the rank of fitness			
 *
 *****************************************************************************/
int fitness( vector<char> initial )
{
	initial = subGrids( initial); 
	
	int sum = 0; 
	string hold;

	// for each subgrid i
	for ( int i = 0; i < 9; i++ )
	{
		hold.clear(); 	// empty string
		for ( int k = 0; k < 9; k++ )
			hold += initial[i*9 + k];

		for ( int k = 49; k < 58; k++ )
		{
			size_t found;
			found = hold.find_first_of(char(k));
			if(found == string::npos)
			{
				sum++;
			}
		}
		hold.clear();
		for(int k = 0; k < 73;k+=9)
			hold+=initial[i+k];
		for ( int k = 49; k < 58; k++ )
		{
			size_t found;
			found = hold.find_first_of(char(k));
			if(found == string::npos)
			{
				sum++;
			}
		}
	}
	
	return sum; 
}
/**************************************************************************//**
 * @author Brock Benson
 *
 * @par Description: The following function is used to help the sort function 
 * sort puzzles based on their fitness rank. 
 *
 * @param[ in ]		a - the first puzzle 
 * @param[ in ] 	b - the second puzzle
 *
 * @returns 		true - if 'a' is more "fit" 
 * @returns		false - if 'b' is more "fit"
 *****************************************************************************/
bool cmp_by_rank( const puzzle a, const puzzle b )
{
	if ( a.fitness < b.fitness )
		return true; 
	return false; 
}
