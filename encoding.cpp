#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

int encoding( ifstream &fin, vector<char> &givens, vector<int> &givenInts );

int main( int argc, char ** argv ) 
{
	vector<char> givens;
	vector<int> givenInts;
	int dim;
	
	if( argc != 2 )
	{
		cout << "Not the correct number of arguments." << endl;
		return -1;
	}

	ifstream fin( argv[ 1 ] ); 

	if( !fin )
	{
		cout << "Could not open the input file." << endl;
		return -2;
	}

	dim = encoding( fin, givens, givenInts );

	return 0;
}

int encoding( ifstream &fin, vector<char> &givens, vector<int> &givenInts )
{
	char x;
	fin >> x;
	int dim = x - '0';
	int i = 0;
	while( fin >> x )
	{
		givens.push_back( x );
		if( givens[ i ] - '0' < 1 )
			givenInts.push_back( 0 );
		else
			givenInts.push_back( givens[ i ] - '0' );
		i++;
	}

	return dim;
}