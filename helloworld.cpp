/*////////////////////////////////////////////
/	Author: Brock Benson
/	Date  :	2/5/2014
/	
/	Description: 	Hello World of genetic algorithms, produces random strings and 
/			mutates and breeds them until they become "Hello, World!"
/
/	Note: Sorry for the messy code, this is mostly for a working copy, not a nice one!
/
*/////////////////////////////////////////////

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std; 


class organism
{
	public:
	char name[14];
	int  rank;

	organism()
	{
		
		for ( int i = 0; i < 14; i++ )
			this->name[i] = char( rand() % (126-32) + 32 );
			
	}
	organism( organism *x )
	{
		for ( int i = 0; i < 14; i++ )
			this->name[i] = x->name[i]; 


	}
	~organism(){}

	//Print
	void print()
	{
		cout << "Name: " << this->name << "\tRank: " << this->rank << endl; 
	}
	//Mutators
	int  fitness()
	{
		char test[14] = "Hello, World!"; 
		//increases rank based on # of incorrect letters
		//higher rank means worse organism
		this->rank = 0; 

		for( int i = 0; i < 14; i++ ) 
			this->rank += abs(int(this->name[i] - test[i]));	//takes difference between characters

		this->rank = this->rank - 32; //first printable letter starts at 32

		return this->rank; 
	}

	void mutation()
	{
		if ( (rand() % 100) < 25 )
			this->name[rand() % 14] = char( rand() % (126-32) + 32 ); 
	}
	
};

//functions
organism crossover( organism &parent1, organism &parent2 );
bool cmp_by_rank( const organism &a, const organism &b );
void kill_unfit( vector<organism> &pop );

/////////////////////////////////////////////////////////main///////////////////////////////////////////////////////////////////////////////////////////
int main()
{

	int population_size = 2048; 	//number of organisms per generation (lowering this too much prevents you from finding solution )
	int generation_size = 10000; 	//number of generations	(generally finishes between 1500-4500 )

	/* initialize random seed: */
  	srand (time(NULL));

	vector<organism> population; 
	
	//Generate Initial Population
	for( int i = 0; i < population_size; i++)
	{
		organism x; 
		x.mutation();
		x.fitness(); 
		population.push_back( x ); 	
	}
for( int j = 0; j < generation_size; j++ )
{
	//Sort the population by rank
	sort( population.begin(), population.end(), cmp_by_rank); 
	//for ( int i = 0; i < population_size; i++ )
	//	population[i].print(); 
	
	//Kill 50% (25% worst, 25% randomly)
	kill_unfit( population );

	//Breed Organisms
	organism z; 
	int r1, r2;
	vector<organism> breed_pop; 
	for( int i = 0; i < population_size; i++ )
	{
		r1 = rand() % population.size() * .05;	//.05 forces everything to breed with the top 5%
		r2 = rand() % population.size() * .05;	// anything over 10% prevents it from finding the solution or
							// increases search time too long

		z = crossover( population[r1], population[r2] );	//miz genes
		z.mutation(); 
		z.fitness(); 
		breed_pop.push_back( z );
		
	}
	
	//copy breed population into the vector
	for ( int i = 0; i < population_size; i++ )
	{
		population[i] = breed_pop[i];		
	}
	
	//delete buffer population
	breed_pop.erase( breed_pop.begin(), breed_pop.end() ); 
	
	//print best every 100 iterations
	if ( j % 100 == 0 )
		population[0].print(); 
	
	//if we find a solution, print it with the generation number
	if ( population[0].rank == 0 )
	{
		population[0].print(); 
		cout << "Generation " << j << endl; 
		return 0;
	}
		 
}

	return 0; 
}
/////////////////////////////////////////////////////////////main/////////////////////////////////////////////////////////////////////

//used to sort population vector
bool cmp_by_rank( const organism &a, const organism &b )
	{
		if ( a.rank < b.rank )
			return true; 
		return false; 
	}

//kill a specific amount off the bottom, and that same amount randomly
void kill_unfit( vector<organism> &pop )
{	
	int size = pop.size();
	size = size * .10; 

	//kill the weakest
	for ( int i = 0; i < size; i++ )
		pop.pop_back(); 

	//kill randomly
	for ( int i = 0; i < size; i++ )
	{	
		int it = rand() % pop.size();
		pop.erase( pop.begin() + it );
	}
}

organism crossover( organism &parent1, organism &parent2 )
{

	int size = 14; 
	int random = rand() % 14;
	organism child; 

	//creates new organism by splitting 2 parents
	for ( int i = 0; i < size; i++ )
	{
		//Averaging
		child.name[i] = char( (int(parent1.name[i]) + int(parent2.name[i])) /2 ); 
		//averaging seems to yield better results
			
		//Splicing	
		/*
		if( i < random )
			child.name[i] = parent1.name[i];
		else	
			child.name[i] = parent2.name[i];
		*/
	}

	return child;
		
}
